public class BT {
    private class node{
        private int value;
        private node leftNode;
        private node rightNode;
    } 
    public node RootNode;

    public void Insert(int value){
        if(RootNode == null)
            RootNode.value = value;
        else{
            node currentNode = RootNode;
            if(currentNode.leftNode != null)
                currentNode.leftNode.value = value;
            else
                currentNode.rightNode.value = value;

        }

    }
}
