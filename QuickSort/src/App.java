public class App {
    public static void main(String[] args) throws Exception {
        int array[] = new int[10];
        int array1[] = new int[10];
        
        array1[0] = 5;
        array1[1] = 2;
        array1[2] = 1;
        array1[3] = 7;
        array1[4] = 9;
        array1[5] = 8;
        array1[6] = 3;
        array1[7] = 5;
        array1[8] = 0;
        array1[9] = 1;

        array[0] = 10;
        array[1] = 16;
        array[2] = 8;
        array[3] = 12;
        array[4] = 15;
        array[5] = 6;
        array[6] = 3;
        array[7] = 9;
        array[8] = 5;
        array[9] = 0;
        
        QuickSort qs = new QuickSort(array1);
        qs.Sort(array1, 0, array.length-1);
        qs.PrintArr();
    }
}
