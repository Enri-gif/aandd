public class QuickSort {
    private int arr[] = new int[10];
    public QuickSort(int arr[]){
        this.arr = arr;
    }

    public void Sort(int arr[], int low, int high){
        int pivot = arr[low];
        if(low >= high)
            return;
        //if(arr[low] > pivot || pivot > arr[high]){
        if(low < high){
            int partitionIndex = Partition(arr, low, high);
            Sort(arr, low, partitionIndex-1);
            Sort(arr, partitionIndex+1, high);
        }
    }
    public int Partition(int arr[], int low, int high){
        int pivot = low;
        int increment = low+1;
        int decrement = high;
        
        while(increment <= decrement){
            if(!moreThanPivot(arr, increment, pivot))
                increment++;
            if(!lessThanPivot(arr, decrement, pivot))
                decrement--;
            if(moreThanPivot(arr, increment, pivot) && lessThanPivot(arr, decrement, pivot) && increment < decrement)
                //if(arr[increment] != arr[decrement])
                    swap(increment, decrement);/*
                if(increment == decrement){
                    increment++;
                    decrement--;
                }*/


        } //while (increment < decrement);
        //if(arr[pivot] >= arr[decrement])
            swap(pivot, decrement);

        return decrement;
    }
    private boolean moreThanPivot(int arr[], int increment, int pivot){
        boolean moreThanPivot = arr[increment] > arr[pivot];
        return moreThanPivot;
    }
    private boolean lessThanPivot(int arr[], int decrement, int pivot){
        boolean lessThanPivot = arr[decrement] <= arr[pivot];
        return lessThanPivot;
    }
    public void swap(int i, int partitionIndex){
        int temp = arr[i];
        arr[i] = arr[partitionIndex];
        arr[partitionIndex] = temp;
    }
    public void PrintArr(){
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
