public class BFS {

    public void Search(int[][] graf){
        int count = graf.length;
        Boolean[] visitedVertex = new Boolean[count];

        for (int i = 0; i < count; i++) {
            visitedVertex[i] = false;
        }

        for(int i = 0; i < count; i++){
            for(int j = 0; j < count; j++){
                if(graf[i][j] != 0 && !visitedVertex[j]){
                    visitedVertex[j] = true;
                }
            }
        }
    }
    
}
