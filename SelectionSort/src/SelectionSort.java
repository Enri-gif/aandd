public class SelectionSort {
    int arr[] = new int[10];
    static final int bigNum = 100;

    public void Sort(int arr[]){
        this.arr = arr;
        for (int j = 0; j < arr.length; j++) {
            int lowestValue = bigNum;
            int lowestIndex = 0;
            for(int i = 0 + j; i < arr.length; i++){
                if(arr[i] < lowestValue){
                    lowestValue = arr[i];
                    lowestIndex = i;
                }
    
            }
            arr[lowestIndex] = arr[j];
            arr[j] = lowestValue;
        }
        
    }
    public void PrintArr(){
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
