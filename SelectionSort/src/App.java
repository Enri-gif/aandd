public class App {
    public static void main(String[] args) throws Exception {
        int array[] = new int[10];
        array[0] = 5;
        array[1] = 2;
        array[2] = 1;
        array[3] = 7;
        array[4] = 9;
        array[5] = 8;
        array[6] = 3;
        array[7] = 5;
        array[8] = 0;
        array[9] = 1;

        SelectionSort ss = new SelectionSort();
        ss.Sort(array);
        ss.PrintArr();
    }
}
