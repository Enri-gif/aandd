import java.util.ArrayList;
import java.util.Collections;

public class A{

    private String dist;
    private ArrayList<Integer> path = new ArrayList<>();
    private int[][] graph;
    private int count;
    private int[] distanceArr;
    private int start;
    private int stop;
    private boolean pathComplete;

    public ArrayList<Integer> GetPath(){
        return path;
    }

    public String getDist(){
        return dist;
    }
    public void setDist(String dist){
        this.dist = dist;
    }

    private void setGraph(int[][] graph){
        this.graph = graph;
    }
    private int[][] getGraph(){
        return graph;
    }

    private int getGraphValue(int i, int j){
        return graph[i][j];
    }

    private void setCount(int count){
        this.count = count;
    }
    private int getCount(){
        return count;
    }

    private void setDistanceArr(int[] distanceArr){
        this.distanceArr = distanceArr;
    }
    public int[] GetDistanceArr(){
        return distanceArr;
    }

    public int GetDistanceArrLen(){
        return distanceArr.length;
    }

    public void SetDistance(int node, int distance){
        distanceArr[node] = distance;
    }
    public int GetDistance(int node){
        return distanceArr[node] + calcHeuristic(node);
    }

    public void SetStart(int start){
        this.start = start;
    }
    public int GetStart(){
        return start;
    }

    public void SetStop(int stop){
        this.stop = stop;
    }
    public int GetStop(){
        return stop;
    }

    private void setPathComplete(boolean pathComplete){
        this.pathComplete = pathComplete;
    }
    private boolean getPathComplete(){
        return pathComplete;
    }
    private int calcHeuristic(int node){
        return Math.abs(GetStop()-node);
    }


    public A(int[][] graph, int start, int stop) {
        SetStart(start);
        SetStop(stop);
        setGraph(graph);
        setCount(graph.length);
    }

    // Finding the minimum distance
    private int findMinDistance(int[] distance, boolean[] visitedVertex) {
        int minDistance = Integer.MAX_VALUE;
        int minDistanceVertex = -1;
        for (int i = 0; i < distance.length; i++) {
            if (!visitedVertex[i] && distance[i] < minDistance/* && calcHeuristic(i) <= minDistance */) {
                minDistance = distance[i];
                minDistanceVertex = i;
            }
        }
        return minDistanceVertex;
    }

    public void CalcDist() {
        int k = GetStop();
        boolean[] visitedVertex = new boolean[getCount()];
        int[] distance = new int[getCount()];
        setDistanceArr(distance);
        for (int i = 0; i < getCount(); i++) {
            visitedVertex[i] = false;
            SetDistance(i, Integer.MAX_VALUE);
        }

        // Distance of self loop is zero
        SetDistance(GetStart(), 0);
        setPathComplete(false);
        for (int i = 0; i < getCount(); i++) {

            // Update the distance between neighbouring vertex and source vertex
            int node = findMinDistance(GetDistanceArr(), visitedVertex);
            visitedVertex[node] = true;
            // Update all the neighbouring vertex distances
            for (int nextNode = 0; nextNode < getCount(); nextNode++) {
                if (!visitedVertex[nextNode] && getGraphValue(node, nextNode) != 0/* && (GetDistance(node) + getGraphValue(node, nextNode) < GetDistance(nextNode)) */){
                        SetDistance(nextNode, (GetDistance(node) + getGraphValue(node, nextNode)));
                }
            }
        }
        while(!getPathComplete()){
            for (int v = 0; v < getCount(); v++) {
                if (getGraphValue(v, k) != 0 && (GetDistance(v) < GetDistance(k))) {
                    if(!path.contains(k)){
                        path.add(k);
                        k = v;
                    }
                }
            }
            
            /*
            if(path.size() == GetDistance(GetStop()) && path.contains(GetStart()) && path.contains(GetStop()))
            setPathComplete(true);
            else if(path.size() == GetDistance(GetStop()) && path.contains(GetStop()))
            setPathComplete(true);
            */

                if(path.contains(start+1) || path.contains(start+(int)Math.sqrt(graph.length)))
                setPathComplete(true);
        }
    }
    public void PrintPath(){
        for (int node : GetPath()) {
            System.out.println(node);
        }
    }
}