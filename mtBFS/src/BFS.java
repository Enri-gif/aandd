public class BFS{
    private int[][] graf;
    public void SetGraf(int[][] graf){
        this.graf = graf;
    }
    public int[][] GetGraf(){
        return graf;
    }

    public int GetCount(){
        return graf.length;
    }

    private Boolean[] visitedVertex = new Boolean[GetCount()];
    public void SetVisVer(){
        for (int i = 0; i < GetCount(); i++) {
            visitedVertex[i] = false;
        }
    }

    public BFS(int[][] graf){
        SetGraf(graf);
        SetVisVer();
    }

    public void Search(){
        
        for(int i = 0; i < GetCount(); i++){
            for(int j = 0; j < GetCount(); j++){
                if(graf[i][j] != 0 && !visitedVertex[j]){
                    visitedVertex[j] = true;
                }
            }
        }
    }
    
}
