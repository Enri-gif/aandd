public class App {
    public static long TabFib(int n){
        long F[] = new long[n+1];
        if(n <= 1)
            return n;
        F[0] = 0;
        F[1] = 1;
        for(int i = 2; i <= n; i++)
            F[i] = F[i-2] + F[i-1];
            
        return F[n];
    }

    public static long Fib(int n){
        if(n <= 1)
            return n;
        return Fib(n-2)+Fib(n-1);
    }
    private static long F[] = new long[50];
    //create negative array
    public static long[] NFib(int n){
        for(int i = 0; i <= n; i++)
            F[i] = -1;

        return F;
    }
    //get from negative array
    public static long GetNFib(int n){
        return F[n];
    }
    //set negative array
    public static void SetNFib(int position, int value){
        F[position] = value;
    }
    public static long MoiFib(int n){
        if(n <= 1)
            return n;
        
        if(GetNFib(n) != -1)
            return GetNFib(n);
        else
            return MoiFib(n-2) + MoiFib(n-1);
    }
    public static void main(String[] args) throws Exception {
        //System.out.println(TabFib(100));
        //System.out.println(Fib(45));
        //System.out.println(MoiFib(45));
    }
}
