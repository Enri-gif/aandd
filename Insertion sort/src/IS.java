public class IS {
    int arr[] = new int[10];

    public void Sort(int arr[]){
        this.arr = arr;
        int pos;
            for(int pos2 = 1; pos2 < arr.length; pos2++){
                for(int iterator = pos2; iterator >= 0; iterator--){
                    pos = iterator-1;
                    if(iterator > 0 && compare(pos, iterator))
                        swap(pos, iterator);

                }
            }
    }
    private boolean compare(int pos, int pos2){
        boolean isBigger;
        if(arr[pos] > arr[pos2])
            isBigger = true;
        else
            isBigger = false;
        return isBigger;
    }
    private void swap(int pos, int pos2){
        int temp = arr[pos];
        arr[pos] = arr[pos2];
        arr[pos2] = temp;
    }
    public void PrintArr(){
        for(int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }
    public boolean unSorted(int unSorted){
        boolean isUnsorted;
        if(unSorted < arr.length)
            isUnsorted = true;
        else
            isUnsorted = false;
        return isUnsorted;
    }

    //alternate implementation
    /*
    while(unSorted(unSorted)){
        for(int pos2 = 1; pos2 < arr.length; pos2++){
            pos = pos2-1;
            if(compare(pos, pos2)){
                swap(pos, pos2);
                unSorted = 0;
            }
            else
                unSorted++;
        }

    }
    */
}
