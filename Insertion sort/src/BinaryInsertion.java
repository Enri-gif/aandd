import java.util.Arrays;

public class BinaryInsertion {
    int arr[] = new int[10];

    public void Sort(int arr[]){
        this.arr = arr;
        int pos;
            for(int pos2 = 1; pos2 < arr.length; pos2++){
                search(pos2);
            }
    }
    private void search(int pos2){
        int subArr[] = Arrays.copyOfRange(arr, 0, pos2+1);
        int left = 0;
        int right = subArr.length-2;
        int middle = setMiddle(right, left);
        
        //condition to keep searching
        do {
            if(arr[pos2] > subArr[right])
                return;
            if(arr[pos2] < subArr[middle]){
                    right = middle;
                    middle = setMiddle(right, left);
            }
            else{
                    left = middle;
                    middle = setMiddle(right, left);
            }
            if(arr[pos2] >= subArr[middle] && arr[pos2] < subArr[right]){
                middle = right;
                left = right;
            }
        } while (!(right == middle && middle == left));

        insert(arr[pos2], middle, subArr);
    }
    private int setMiddle(int right, int left){
        int middle= Math.round((right+left)/2);
        return middle;
    }
    private void insert(int item, int position, int subArr[]){
        if(subArr[subArr.length-1] < subArr[subArr.length-2])
            for(int i = subArr.length-1; i > position; i--)
                subArr[i] = subArr[i-1];
        else
            return;

        subArr[position] = item;
        for (int element = 0; element < subArr.length; element++) {
            arr[element] = subArr[element];
        }
    }
    private void swap(int pos, int pos2){
        int temp = arr[pos];
        arr[pos] = arr[pos2];
        arr[pos2] = temp;
    }
    public void PrintArr(){
        for(int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
    }
    public boolean unSorted(int unSorted){
        boolean isUnsorted;
        if(unSorted < arr.length)
            isUnsorted = true;
        else
            isUnsorted = false;
        return isUnsorted;
    }
}

//!(arr[pos2] < subArr[middle] && arr[pos2] > subArr[left]) || !(arr[pos2] > subArr[middle] && arr[pos2] < subArr[right]) || !(right == middle && middle == left)