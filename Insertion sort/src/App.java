public class App {
    public static void main(String[] args) throws Exception {
        int array[] = new int[10];
        array[0] = 5;
        array[1] = 2;
        array[2] = 1;
        array[3] = 7;
        array[4] = 9;
        array[5] = 8;
        array[6] = 3;
        array[7] = 5;
        array[8] = 0;
        array[9] = 1;
        //Insertion sort

        /*
        IS is = new IS();
        is.Sort(array);
        is.PrintArr();
        */

        //Binary insetion
        BinaryInsertion bi = new BinaryInsertion();
        bi.Sort(array);
        bi.PrintArr();
    }
}
