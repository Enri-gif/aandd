import java.util.ArrayList;
import java.util.List;

public class Dij {
    public String dist;
    public ArrayList<Integer> path;
    public List<ArrayList<Integer>> pathList;
    public int startDist;
    public int stopDist;
    public void CalcDist(int[][] graph, int start, int stop) {
        boolean pathComplete = false;
        int k = stop;
        pathList = new ArrayList<>();
        path = new ArrayList<>();
        startDist = start;
        stopDist = stop;
        int count = graph.length;
        boolean[] visitedVertex = new boolean[count];
        int[] distance = new int[count];
        for (int i = 0; i < count; i++) {
            visitedVertex[i] = false;
            distance[i] = Integer.MAX_VALUE;
        }

        // Distance of self loop is zero
        distance[start] = 0;
        while(!pathComplete){
//            if(k%Math.sqrt(graph.length) > stop%Math.sqrt(graph.length))
//                k = k/(stop%(int)Math.sqrt(graph.length)+1);
            for (int i = 0; i < count; i++) {
                visitedVertex[i] = false;
                distance[i] = Integer.MAX_VALUE;
            }
            distance[start] = 0;
            for (int i = 0; i < count; i++) {

                // Update the distance between neighbouring vertex and source vertex
                int u = findMinDistance(distance, visitedVertex);
                visitedVertex[u] = true;
                // Update all the neighbouring vertex distances
                for (int v = 0; v < count; v++) {
                    if (!visitedVertex[v] && graph[u][v] != 0 && (distance[u] + graph[u][v] < distance[v])) {
                        distance[v] = distance[u] + graph[u][v];
                        //update path array
                        /*if(path.isEmpty()){
                            if(v == stop)
                                path.add(v);
                        }*/
                        if(graph[u][k] != 0 && distance[u] < distance[k] && !path.contains(k)){
                            path.add(k);
                            k = u;
                        }
                    }
                }
            }
            if(path.size() == distance[stop] && path.contains(start) && path.contains(stop))
                pathComplete = true;
            else if(path.size() == distance[stop] && path.contains(stop))
                pathComplete = true;
        }
        for (int i = 0; i < distance.length; i++) {
            if(i != start && i == stop)
                dist = String.format("Distance from %s to %s is %s", start, stop, distance[i]);
        }
    }

    // Finding the minimum distance
    private static int findMinDistance(int[] distance, boolean[] visitedVertex) {
        int minDistance = Integer.MAX_VALUE;
        int minDistanceVertex = -1;
        for (int i = 0; i < distance.length; i++) {
            if (!visitedVertex[i] && distance[i] < minDistance) {
                minDistance = distance[i];
                minDistanceVertex = i;
            }
        }
        return minDistanceVertex;
    }
}
