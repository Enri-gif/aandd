public class Prim {

    int Visited[] = new int[10];
    Pq pq = new Pq();
    int MST[][] = new int[10][10];
    
    public int[][] CreateGraph(){
        int graph[][] = new int[10][10];
        graph[0][1] = 1;
        graph[1][0] = 1;
        graph[0][3] = 2;
        graph[3][0] = 2;
        graph[1][2] = 4;
        graph[2][1] = 4;
        graph[2][5] = 7;
        graph[5][2] = 7;
        graph[1][4] = 6;
        graph[4][1] = 6;
        graph[3][4] = 2;
        graph[4][3] = 2;
        graph[4][5] = 9;
        graph[5][4] = 9;
        graph[4][7] = 5;
        graph[7][4] = 5;
        graph[5][6] = 3;
        graph[6][5] = 3;
        graph[7][8] = 9;
        graph[8][7] = 9;
        graph[8][9] = 3;
        graph[9][8] = 3;
/*
        for (int[] is : graph) {
            for (int i : is) {
                if(i != 1)
                    i = 0;
            }
        }
*/
        return graph;
    }//expected order [0,1], [0,3], [3,4], [1,2], [4,7], [2,5], [5,6], [7,8], [8,9]
    public int[][] FindMST(int[][] graph, int start){
        //int start = 0;
        
        Visited[start] = 1;
        
        

            //enqueue 1, 3 add 1 enqueue 2, 4 add 3, enqueue 4 add [3,4] enqueue 7, 5 add 2 enqueue 5
            for (int[] edges : graph) {
                int node = start;
               while(node < Visited.length && Visited[node] == 1){
                    visitNeighbours(graph, node);
                    dequeueVisited();

                    if(!pq.isEmpty() && graph[node][pq.Peak("node")] != 0 && graph[node][pq.Peak("node")] == pq.Peak("priority")){

                            //allNeighboursEnqueued(graph);

                            Visited[pq.Peak("node")] = 1;
                            node = addToMST(node);
                    }
                    else{
                        node = gotoNextVisited(node);
                    }
                }
            }
        
        return MST;
    }
    private int gotoNextVisited(int next){
        int nextVisited = next;
        nextVisited++;
        while(nextVisited < Visited.length && Visited[nextVisited] == 0)
            nextVisited++;
        return nextVisited;
    }
    private void visitNeighbours(int[][] graph, int node){
        for(int neighbour = 0; neighbour < Visited.length && Visited[node] == 1; neighbour++){
            if(graph[node][neighbour] != 0 && Visited[neighbour] != 1){ //iterate neighbours and add to pq
                pq.Enqueu(graph[node][neighbour], neighbour);

            }
        }
    }
    private int addToMST(int node){
        MST[node][pq.Peak("node")] = pq.Peak("priority");
        MST[pq.Peak("node")][node] = pq.Peak("priority");
        return pq.Dequeue("node");
    }
    private void dequeueVisited(){
        boolean peakvisited = true;
        while(peakvisited){
            if(pq.isEmpty())
                return;
            if(Visited[pq.Peak("node")] == 1)
                pq.Dequeue("node");
            else
                peakvisited = false;

        }
    }
    public void PrintMST(int[][] MST){
        for (int[] is : MST) {
            for (int i : is) {
                System.out.print(i);
            }
            System.out.print("\n");
        }
    }
}
