public class Pq {
    public class Node{
        private int priority = 999;
        private int msg;
        private Node next;
        private int Index;
    }
    private Node headNode = new Node();
    private Node current;
    public int nodeCount = 0;

    public void Enqueu(int Priority, int Msg){
        current = headNode;
        for(int i = 0; i <= nodeCount; i++){
            if(i == nodeCount){
                Node n = new Node();
                n.msg = Msg;
                n.Index = i;
                n.priority = Priority;
                current.next = n;
            }
            current = current.next;
        }
        nodeCount++;
    }
    public void DeleteAt(int Index){
        current = headNode;
        if(Index <= nodeCount){
            for(int i = 0; i <= Index; i++){
                if(Index == i){
                    if(current.next != null)
                        current.next = current.next.next;
                    nodeCount--;
                }
                current = current.next;
            }
            reOrderQueue();
        }
        else throw new IndexOutOfBoundsException();
    }
    public int Peak(String returnValue){
        current = headNode;
        Node highP = current;
        for(int i = 0; i <= nodeCount; i++){
            if(current.next != null && current.next.priority < highP.priority)
                highP = current.next;
            current = current.next;
        }
        return returnValue(returnValue, highP.priority, highP.msg);
    }
    public int Dequeue(String returnValue){
        if(!isEmpty()){
            current = headNode;
            Node highP = current;
            for(int i = 0; i <= nodeCount; i++){
                if(current.next != null && current.next.priority < highP.priority)
                    highP = current.next;
                current = current.next;
            }
            DeleteAt(highP.Index);
            return returnValue(returnValue, highP.priority, highP.msg);
        }
        else throw new IndexOutOfBoundsException();
    }
    private void reOrderQueue(){
        current = headNode;
        for(int i = 0; i <= nodeCount; i++){
            if(current.next != null)
                current.next.Index = i;
            current = current.next;
        }
    }
    public boolean isEmpty(){
        boolean isEmpty = false;
        if(nodeCount <= 0)
            isEmpty = true;

        return isEmpty;
    }
    private int returnValue(String returnValue, int priority, int msg){
        int Value;
        if(returnValue == "node")
            Value = msg;
        else
            Value = priority;
        return Value;
    }
}
