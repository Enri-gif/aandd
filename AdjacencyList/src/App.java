public class App {
    public static void main(String[] args) throws Exception {
        AdjacencyList aList = new AdjacencyList();
        aList.AddNode(16);
        aList.AddEdge(2,8);
        aList.AddEdge(5, 15);

        aList.PrintList();
    }
}
