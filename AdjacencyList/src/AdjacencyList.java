import java.util.ArrayList;
import java.util.List;

public class AdjacencyList {
    public ArrayList<List<Integer>> AdList;

    public AdjacencyList(){
        AdList = new ArrayList<>();

        for(int i = 0; i < 8; i++){
            //create a new list that has the adjacent nodes
            //Add list to AdList
            //repeat until size
            List<Integer> list = new ArrayList<Integer>();
            list.add(i+1);
            list.add(i+8);
            AdList.add(list);
        }
    }
    public void AddNode(int node){
        List<Integer> list = new ArrayList<Integer>();
        list.add(node-1);
        AdList.add(list);
    }
    public void AddEdge(int from, int to){
        AdList.get(from).add(to);
    }
    public void PrintList(){
        for (List<Integer> list : AdList) {
            for (Integer integer : list) {
                System.out.print(integer + " ");
            }
            System.out.print("\n");
        }
    }
}
