import java.util.ArrayList;
import java.util.List;

public class MergeSort {
    public List<Integer> Merge(List<Integer> ll, List<Integer> rl){

        List<Integer> result = new ArrayList<>();
        int biggestListSize;

        if(ll.size() > rl.size())
            biggestListSize = ll.size();
        else
            biggestListSize = rl.size();

        int LeftIterator = 0;
        int RightIterator = 0;
        while(LeftIterator < biggestListSize && RightIterator < biggestListSize){
            if(LeftIterator < ll.size() && ll.get(LeftIterator) <= rl.get(RightIterator)){
                result.add(ll.get(LeftIterator));
                LeftIterator++;
            }
            else{
                result.add(rl.get(RightIterator));
                RightIterator++;
            }

        }
        while(LeftIterator < ll.size()){
            result.add(ll.get(LeftIterator));
            LeftIterator++;
        }

        while(RightIterator < rl.size()){
            result.add(rl.get(RightIterator));
            RightIterator++;
        }
        return result;
    }
    public List<Integer> Sort(List<Integer> list){
        if(list.size() == 1)
            return list;
        //split list until simgle element lists
        int middle = list.size()/2;
        List<Integer> ll = list.subList(0, middle);
        List<Integer> rl = list.subList(middle, list.size());
        //Sort(ll);
        //Sort(rl);
        return Merge(Sort(ll), Sort(rl));
    }
}
