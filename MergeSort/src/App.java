import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        list.add(61);
        list.add(17);
        list.add(45);
        list.add(2);
        list.add(5);
        list.add(8);
        list.add(11);
        list.add(109);
        list.add(21);

        MergeSort ms = new MergeSort();
        
        for (int num : ms.Sort(list)) {
            System.out.println(num);
        }
    }
}
